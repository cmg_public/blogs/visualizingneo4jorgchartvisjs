let viz;

function draw() {
    const config = {
        containerId: "viz",
        neo4j: {
            serverUrl: "bolt://127.0.0.1:7687",
            serverUser: "read_only",
            serverPassword: "read_only123"
        },
        visConfig: {
            nodes: {
            },
            edges: {
                length: 100,
                arrows: {
                    to: {enabled: false}
                },
            },
            physics: {
                hierarchicalRepulsion: {
                    avoidOverlap: 1
                },
                solver: "repulsion",
                repulsion: {
                    nodeDistance: 1000
                }
            },
            layout: {
                improvedLayout: true,
                randomSeed: 420,
                hierarchical: {
                    enabled: true,
                    direction: 'DU',
                    sortMethod: 'directed',
                    nodeSpacing: 1000,
                    treeSpacing: 20,
                    levelSeparation: 250,
                }
            },
        },
        labels: {
            Person: {
                label: "PersonGivenName",
                group: 'Team',
                caption: true,
                [NeoVis.NEOVIS_ADVANCED_CONFIG]: {
                    cypher: {
                        value: "MATCH (n) WHERE id(n) = $id RETURN n.size"
                    },
                    function: {
                        title: NeoVis.objectToTitleHtml,
                        title: (props) => NeoVis.objectToTitleHtml(props, ["PersonCompany","Team","PersonDisplayName", "PersonTelephoneNumber", "PersonMail", "PersonManager_Name","PersonCN"])
                    },
                    static: {
                        shape: 'icon',
                        icon: {
                            face: "'Font Awesome 5 Free'",
                            weight: "900", // Font Awesome 5 doesn't work properly unless bold.
                            code: "\uf007",
                            size: 50,
                            color: "rgba(84,81,81,0.75)",
                        },
                    },
                }
            },
            Company: {
                label: "CompanyName",
                caption: true,
                [NeoVis.NEOVIS_ADVANCED_CONFIG]: {
                    cypher: {
                        value: "MATCH (n) WHERE id(n) = $id RETURN n.size"
                    },
                    function: {
                        title: NeoVis.objectToTitleHtml,
                        title: (props) => NeoVis.objectToTitleHtml(props, ["PersonDisplayName", "PersonTelephoneNumber", "PersonMail", "PersonManager_Name"])
                    },
                    static: {
                        shape: "icon",
                        icon: {
                            face: "'Font Awesome 5 Free'",
                            weight: "bold", // Font Awesome 5 doesn't work properly unless bold.
                            code: "\uf1ad",
                            size: 100,
                            color: "rgb(11,27,131)",
                        },
                    },
                }
            },
            Team: {
                label: "TeamName",
                caption: true,
                [NeoVis.NEOVIS_ADVANCED_CONFIG]: {
                    cypher: {
                        value: "MATCH (n) WHERE id(n) = $id RETURN n.size"
                    },
                    function: {
                        title: NeoVis.objectToTitleHtml,
                        title: (props) => NeoVis.objectToTitleHtml(props, ["TeamName"])
                    },
                    static: {
                        shape: "icon",
                        icon: {
                            face: "'Font Awesome 5 Free'",
                            weight: "900", // Font Awesome 5 doesn't work properly unless bold.
                            code: "\uf0c0",
                            size: 75,
                            color: "#f57842",
                        },
                    },
                }
            }

        },
        relationships: {
            "MANAGES": {
                caption: true,
                label: "name",
                group: "community",
                [NeoVis.NEOVIS_ADVANCED_CONFIG]: {
                    cypher: {
                        value: "MATCH (n) WHERE id(n) = $id RETURN n.size"
                    },
                    function: {
                        title: NeoVis.objectToTitleHtml,
                        title: (props) => NeoVis.objectToTitleHtml(props, ["hover"])
                    },
                }
            },
            "IS_EMPLOYEE_OF": {
                caption: true,
                label: "name",
                group: "community",
                [NeoVis.NEOVIS_ADVANCED_CONFIG]: {
                    cypher: {
                        value: "MATCH (n) WHERE id(n) = $id RETURN n.size"
                    },
                    function: {
                        title: NeoVis.objectToTitleHtml,
                        title: (props) => NeoVis.objectToTitleHtml(props, ["hover"])
                    },
                }
            },
            "PART_OF": {
                caption: true,
                label: "name",
                group: "community",
                [NeoVis.NEOVIS_ADVANCED_CONFIG]: {
                    cypher: {
                        value: "MATCH (n) WHERE id(n) = $id RETURN n.size"
                    },
                    function: {
                        title: NeoVis.objectToTitleHtml,
                        title: (props) => NeoVis.objectToTitleHtml(props, ["hover"])
                    },
                }
            },
            "MEMBER_OF": {
                caption: true,
                label: "name",
                group: "community",
                [NeoVis.NEOVIS_ADVANCED_CONFIG]: {
                    cypher: {
                        value: "MATCH (n) WHERE id(n) = $id RETURN n.size"
                    },
                    function: {
                        title: NeoVis.objectToTitleHtml,
                        title: (props) => NeoVis.objectToTitleHtml(props, ["hover"])
                    },
                }
            }
        },
        initialCypher: "match(p)-[r]-(c) return p,r,c limit 500"
    };

    viz = new NeoVis.default(config);
    viz.render();
    console.log(viz);

}